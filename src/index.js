#!/usr/bin/env node
'use strict';

const Route = require('./router/Route')

// 1. Parse the argument(s)
const workingDirectory = function() {
    const args = process.argv.slice(2);
    if (args.length > 0) {
        return args[0]
    }

    return process.cwd()
}()

// 2. Start the Kernel to boot the services
const AppKernel = require('./kernel/AppKernel')
const serviceProviders = require('./config/serviceProviders')

AppKernel.build(workingDirectory, serviceProviders).then((appKernel) => {
    // 3. Boot the controller
    appKernel.boot()
    const router = appKernel.container.use('cursed/router/Router')
    router.route(new Route('/'))
})
