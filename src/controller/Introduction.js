'use strict'

const Route = require('../router/Route')

class Introduction {
    constructor(eventEmitter, inquirer, configurationStore) {
        this.eventEmitter = eventEmitter
        this.inquirer = inquirer
        this.configurationStore = configurationStore
    }

    action() {
        const route = new Route('search')
        const githubToken = this.configurationStore.get('github_token')

        if (undefined !== githubToken) {
            this.eventEmitter.emit('reroute', route)
            return
        }

        this.askForGithubToken((githubToken) => {
            this.configurationStore.set('github_token', githubToken);

            this.eventEmitter.emit('reroute', route)
        })
    }

    askForGithubToken(callback) {
        const question = {
            type: 'input',
            name: 'github_token',
            message: 'Might we trouble you for your GitHub API token?',
        }
        this.inquirer.prompt([question]).then((answers) => {
            callback(answers.github_token)
        })
    }
}

module.exports = Introduction
