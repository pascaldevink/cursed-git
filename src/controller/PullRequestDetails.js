'use strict'

class PullRequestDetails {
    constructor(githubApiClient, pullRequestDetails) {
        this.githubApiClient = githubApiClient
        this.pullRequestDetails = pullRequestDetails
    }

    action(pullRequest) {
        this.githubApiClient.getPullRequestInformation(
            pullRequest.repositoryOwner,
            pullRequest.repository,
            pullRequest.number
        ).then((fullPullRequest) => {
            this.pullRequestDetails.render(fullPullRequest)
        })
    }
}

module.exports = PullRequestDetails
