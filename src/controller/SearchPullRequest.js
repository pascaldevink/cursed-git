'use strict'

const Route = require('../router/Route')

class SearchPullRequest {
    constructor(eventEmitter, githubApiClient, loadingScreen, messageDialog, searchPullRequest) {
        this.eventEmitter = eventEmitter
        this.githubApiClient = githubApiClient
        this.loadingScreen = loadingScreen
        this.messageDialog = messageDialog
        this.searchPullRequest = searchPullRequest
        this.searchQueryPrompt = use('cursed/view/SearchQueryPrompt')
        this.configStore = use('cursed/service/ConfigurationStore')

        this.pullRequests = []
        this.selectedPullRequest = null

        this.setupListeners()
    }

    setupListeners() {
        this.eventEmitter.on('select pull request', (index) => { this.handleSelectEvent(index) })
        this.eventEmitter.on('display details', () => { this.displayDetailsSceen() })
        this.eventEmitter.on('checkout branch', () => { this.checkoutSelectedPullRequest() })
        this.eventEmitter.on('refresh list', () => { this.refreshList() })
        this.eventEmitter.on('prompt search query', () => { this.askForNewSearchQuery() })
        this.eventEmitter.on('update search query', (searchQuery) => { this.updateSearchQuery(searchQuery) })
    }

    action() {
        this.search((pullRequests) => this.renderList(pullRequests))
    }

    search(callback) {
        const loadingScreen = this.loadingScreen.render('Search pull requests')
        const searchLine = this.configStore.get('search_query', 'is:pr is:open')

        this
            .githubApiClient
            .search(searchLine)
            .then((pullRequests) => {
                loadingScreen.stop()
                this.pullRequests = pullRequests
                callback(pullRequests)
            })
    }

    renderList(pullRequests) {
        this.searchPullRequest.render(
            pullRequests,
            this.labelColors()
        )
    }

    labelColors() {
        const labelColors = this.configStore.get('label_colors', [])

        return new Map(Object.entries(labelColors));
    }

    refreshList() {
        this.search((pullRequests) => this.renderList(pullRequests))
    }

    handleSelectEvent(index) {
        this.selectedPullRequest = this.pullRequests[index]
    }

    displayDetailsSceen() {
        if (false === this.hasSelectedPullRequest()) {
            return;
        }

        this.eventEmitter.emit('reroute', new Route('pullrequest', this.selectedPullRequest))
    }

    checkoutSelectedPullRequest() {
        if (false === this.hasSelectedPullRequest()) {
            return
        }

        const loadingScreen = this.loadingScreen.render('Checking out the branch...')

        this
            .githubApiClient
            .checkoutPullRequest(this.selectedPullRequest)
            .then(() => {
                loadingScreen.stop()
                this.messageDialog.render('Branch checked out', 5)
            })
    }

    hasSelectedPullRequest() {
        if (undefined === this.selectedPullRequest) {
            return false
        }

        if (null === this.selectedPullRequest) {
            return false
        }

        return true
    }

    askForNewSearchQuery() {
        this.searchQueryPrompt.render(this.configStore.get('search_query', 'is:pr is:open'));
    }

    updateSearchQuery(searchQuery) {
        this.configStore.set('search_query', searchQuery)
        this.refreshList()
    }
}

module.exports = SearchPullRequest
