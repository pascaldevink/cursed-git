'use strict'

class Route {
    constructor(name, parameters) {
        this._name = name
        this._parameters = Array.isArray(parameters) ? parameters : [parameters]
    }

    get name() {
        return this._name
    }

    get parameters() {
        return this._parameters
    }
}

module.exports = Route
