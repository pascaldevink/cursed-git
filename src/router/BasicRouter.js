'use strict';

class BasicRouter {
    constructor(container, routes) {
        this.container = container
        this.routes = routes
    }

    route(route) {
        const controllerName = this.routes[route.name]
        this.container.use(controllerName).action(...route.parameters)
    }
}

module.exports = BasicRouter;
