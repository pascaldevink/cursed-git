const path = require('path')

module.exports = [
    // Services
    path.join(__dirname, '../kernel/RouterServiceProvider.js'),
    path.join(__dirname, '../kernel/EventEmitterServiceProvider.js'),
    path.join(__dirname, '../kernel/ConfigurationServiceProvider.js'),
    path.join(__dirname, '../kernel/GithubServiceProvider.js'),
    path.join(__dirname, '../kernel/InquirerServiceProvider.js'),

    // Views
    path.join(__dirname, '../kernel/BlessedServiceProvider.js'),
    path.join(__dirname, '../kernel/ViewServiceProvider.js'),

    // Controllers
    path.join(__dirname, '../kernel/ControllerServiceProvider.js'),
]
