'use strict';

class ConfigurationStore {
    constructor(store) {
        this.store = store
    }

    set(key, value) {
        this.store.set(key, value)
    }

    has(key) {
        return this.store.has(key)
    }

    get(key, defaultValue) {
        if (false == this.has(key)) {
            return defaultValue
        }

        return this.store.get(key)
    }
}

module.exports = ConfigurationStore;
