'use strict';

const SearchResult = require('../model/SearchResult')
const PullRequest = require('../model/PullRequest')

class GithubApiClient {
    constructor(githubApi, gitClient) {
        this.githubApi = githubApi
        this.gitClient = gitClient
    }

    search(searchLine) {
        return new Promise((resolve, reject) => {
            this.githubApi.search.issues({
                q: searchLine
            }, function(err, res) {
                if (null !== err) {
                    reject(err)
                    return
                }

                const pullRequests = [];

                for (const pr of res.items) {
                    const searchResult = new SearchResult(
                        pr.number,
                        pr.user.login,
                        pr.repository_url,
                        pr.title,
                        pr.body,
                        pr.labels.map((label) => { return label.name })
                    )

                    pullRequests.push(searchResult)
                }

                resolve(pullRequests);
            })
        })
    }

    getPullRequestInformation(owner, repo, number) {
        return new Promise((resolve, reject) => {
            this.githubApi.pullRequests.get({
                owner: owner,
                repo: repo,
                number: number,
            }, (err, response) => {
                if (null === err) {
                    const pullRequest = new PullRequest(
                        response.number,
                        response.user.login,
                        response.repository_url,
                        response.title,
                        response.body,
                        response.head.ref
                    )

                    resolve(pullRequest)
                } else {
                    reject(err)
                }
            })
        })
    }

    checkoutPullRequest(pullRequest) {
        return new Promise((resolve, reject) => {
            this.getPullRequestInformation(
                pullRequest.repositoryOwner,
                pullRequest.repository,
                pullRequest.number
            ).then((pullRequest) => {
                    this.gitClient.fetch((err, result) => {
                        if (err !== null) {
                            reject(err)
                            return
                        }

                        this.gitClient.checkout(pullRequest.branch, (err, result) => {
                            if (err !== null) {
                                reject(err)
                                return
                            }

                            resolve()
                        })
                    })
                }
            ).catch((error) => {
                reject(error)
            })
        })
    }
}

module.exports = GithubApiClient
