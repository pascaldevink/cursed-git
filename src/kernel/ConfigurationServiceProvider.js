'use strict'

const ServiceProvider = require('adonis-fold').ServiceProvider

class ConfigurationServiceProvider extends ServiceProvider {
    * register () {
        this.app.singleton('cursed/service/ConfigurationStore', (app) => {
            const Configstore = require('configstore')
            const conf = new Configstore('cursed-git')
            const ConfigurationStore = require('../service/ConfigurationStore')

            return new ConfigurationStore(conf)
        })
    }
}

module.exports = ConfigurationServiceProvider
