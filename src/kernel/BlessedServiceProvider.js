'use strict'

const ServiceProvider = require('adonis-fold').ServiceProvider

class BlessedServiceProvider extends ServiceProvider {
    * register() {
        this.app.singleton('cursed/view/blessed', (app) => {
            return require('blessed')
        })

        this.app.singleton('cursed/view/blessed-contrib', (app) => {
            return require('blessed-contrib')
        })

        this.app.singleton('cursed/view/screen', (app) => {
            return app.use('cursed/view/blessed').screen({
                smartCSR: true,
                debug: true,
            })
        })

        this.app.singleton('cursed/view/chalk', (app) => {
            return require('chalk')
        })
    }

    * boot() {
        this.app.use('cursed/view/screen').key(['q', 'C-c'], (ch, key) => {
            return process.exit(0)
        })
    }
}

module.exports = BlessedServiceProvider
