'use strict'

const ServiceProvider = require('adonis-fold').ServiceProvider

class ControllerServiceProvider extends ServiceProvider {
    * register () {
        this.app.bind('cursed/controller/Introduction', (app) => {
            const Introduction = require('../controller/Introduction')
            return new Introduction(
                app.use('cursed/event/EventEmitter'),
                app.use('cursed/service/inquirer'),
                app.use('cursed/service/ConfigurationStore')
            )
        })

        this.app.bind('cursed/controller/SearchPullRequest', (app) => {
            const SearchPullRequest = require('../controller/SearchPullRequest')
            return new SearchPullRequest(
                app.use('cursed/event/EventEmitter'),
                app.use('cursed/service/GithubApiClient'),
                app.use('cursed/view/LoadingScreen'),
                app.use('cursed/view/MessageDialog'),
                app.use('cursed/view/SearchPullRequest')
            )
        })

        this.app.bind('cursed/controller/PullRequestDetails', (app) => {
            const PullRequestDetails = require('../controller/PullRequestDetails')
            return new PullRequestDetails(
                app.use('cursed/service/GithubApiClient'),
                app.use('cursed/view/PullRequestDetails')
            )
        })
    }
}

module.exports = ControllerServiceProvider
