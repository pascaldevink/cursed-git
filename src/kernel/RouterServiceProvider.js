'use strict'

const ServiceProvider = require('adonis-fold').ServiceProvider

class RouterServiceProvider extends ServiceProvider {
    * register() {
        this.app.singleton('cursed/router/BasicRouter', (app) => {
            const routesConfiguration = require('../config/routes.json')
            const BasicRouter = require('../router/BasicRouter')
            return new BasicRouter(app, routesConfiguration)
        })

        this.app.alias('cursed/router/Router', 'cursed/router/BasicRouter')
    }
}

module.exports = RouterServiceProvider
