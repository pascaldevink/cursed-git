'use strict'

const ServiceProvider = require('adonis-fold').ServiceProvider

class EventEmitterServiceProvider extends ServiceProvider {
    * register() {
        this.app.singleton('cursed/event/EventEmitter', (app) => {
            const EventEmitter = require('events')
            return new EventEmitter()
        })
    }
}

module.exports = EventEmitterServiceProvider
