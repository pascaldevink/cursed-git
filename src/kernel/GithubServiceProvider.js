'use strict'

const ServiceProvider = require('adonis-fold').ServiceProvider

class GithubServiceProvider extends ServiceProvider {
    * register() {
        this.app.singleton('cursed/service/Git', (app) => {
            return require('simple-git')(app.use('cursed/configuration/WorkingDirectory'))
        })

        this.app.singleton('cursed/service/GithubApi', (app) => {
            const GitHubApi = require("github")
            const configurationStore = app.use('cursed/service/ConfigurationStore')

            const github = new GitHubApi({
                // optional
                debug: false,
                protocol: "https",
                host: "api.github.com", // should be api.github.com for GitHub
                pathPrefix: "", // for some GHEs; none for GitHub
                headers: {
                    "user-agent": "cursed-git" // GitHub is happy with a unique user agent
                },
                Promise: require('bluebird'),
                followRedirects: false, // default: true; there's currently an issue with non-get redirects, so allow ability to disable follow-redirects
                timeout: 5000
            })

            github.authenticate({
                type: "token",
                token: configurationStore.get('github_token'),
            })

            return github
        })

        this.app.singleton('cursed/service/GithubApiClient', (app) => {
            const GithubApiClient = require('../service/GithubApiClient')
            return new GithubApiClient(
                app.use('cursed/service/GithubApi'),
                app.use('cursed/service/Git')
            )
        })
    }
}

module.exports = GithubServiceProvider
