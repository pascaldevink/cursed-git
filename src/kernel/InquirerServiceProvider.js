'use strict'

const ServiceProvider = require('adonis-fold').ServiceProvider

class InquirerServiceProvider extends ServiceProvider {
    * register() {
        this.app.singleton('cursed/service/inquirer', (app) => {
            return require('inquirer')
        })
    }
}

module.exports = InquirerServiceProvider
