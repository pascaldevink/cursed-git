'use strict'

const fold = require('adonis-fold')

class AppKernel {
    static build(workingDirectory, providers) {
        const Ioc = fold.Ioc
        const Registrar = fold.Registrar

        Ioc.bind('cursed/configuration/WorkingDirectory', () => {
            return workingDirectory
        })

        return new Promise((resolve, reject) => {
            Registrar
                .register(providers)
                .then(() => {
                    resolve(new AppKernel(Ioc))
                })
        })
    }

    constructor(container) {
        this._container = container
    }

    boot() {
        this._container.use('cursed/event/EventEmitter').on('reroute', (route) => {
            this.reroute(route)
        })
    }

    reroute(route) {
        this._container.use('cursed/router/Router').route(route)
    }

    /**
     * @returns Ioc
     */
    get container() {
        return this._container
    }
}

module.exports = AppKernel
