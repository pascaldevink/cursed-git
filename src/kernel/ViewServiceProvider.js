'use strict'

const ServiceProvider = require('adonis-fold').ServiceProvider

class ViewServiceProvider extends ServiceProvider {
    * register() {
        this.app.singleton('cursed/view/TopLayout', (app) => {
            const TopLayout = require('../view/TopLayout')
            return new TopLayout(app.use('cursed/view/blessed'), app.use('cursed/view/screen'))
        })

        this.app.bind('cursed/view/LoadingScreen', (app) => {
            const LoadingScreen = require('../view/LoadingScreen')
            return new LoadingScreen(
                app.use('cursed/view/blessed'),
                app.use('cursed/view/screen'),
                app.use('cursed/view/TopLayout')
            )
        })

        this.app.bind('cursed/view/MessageDialog', (app) => {
            const MessageDialog = require('../view/MessageDialog')
            return new MessageDialog(
                app.use('cursed/view/blessed'),
                app.use('cursed/view/screen'),
                app.use('cursed/view/TopLayout')
            )
        })

        this.app.bind('cursed/view/SearchPullRequest', (app) => {
            const SearchPullRequest = require('../view/SearchPullRequest')
            return new SearchPullRequest(
                app.use('cursed/event/EventEmitter'),
                app.use('cursed/view/blessed'),
                app.use('cursed/view/screen'),
                app.use('cursed/view/TopLayout'),
                app.use('cursed/view/chalk')
            )
        })

        this.app.bind('cursed/view/SearchQueryPrompt', (app) => {
            const MessageDialog = require('../view/SearchQueryPrompt')
            return new MessageDialog(
                app.use('cursed/event/EventEmitter'),
                app.use('cursed/view/blessed'),
                app.use('cursed/view/screen'),
                app.use('cursed/view/TopLayout')
            )
        })

        this.app.bind('cursed/view/PullRequestDetails', (app) => {
            const PullRequestDetails = require('../view/PullRequestDetails')
            return new PullRequestDetails(
                app.use('cursed/view/blessed'),
                app.use('cursed/view/blessed-contrib'),
                app.use('cursed/view/screen'),
                app.use('cursed/view/TopLayout')
            )
        })
    }
}

module.exports = ViewServiceProvider
