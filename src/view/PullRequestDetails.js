'use strict'

class PullRequestDetails {
    constructor(blessed, blessedContrib, screen, topLayout) {
        this.blessed = blessed
        this.blessedContrib = blessedContrib
        this.screen = screen
        this.topLayout = topLayout
    }

    render(pullRequest) {
        this.screen.realloc()

        const titleBox = this.blessed.text({
            height: 2,
            width: '100%',
            style: {
                fg: 'white',
                bg: 'blue',
            },
            content: pullRequest.title,
        })
        this.screen.append(titleBox)

        const markdownBox = this.blessedContrib.markdown({
            top: 2,
            height: '100%',
        })
        markdownBox.setMarkdown(pullRequest.body)
        markdownBox.focus()

        this.screen.append(markdownBox)

        this.screen.render()

        this.screen.on('keypress', (ch, key) => {
            if ('escape' === key.name) {
                titleBox.destroy()
                markdownBox.destroy()

                this.screen.render()
            }
        })
    }
}

module.exports = PullRequestDetails
