'use strict'

class MessageDialog {
    constructor(blessed, screen, topLayout) {
        this.blessed = blessed
        this.screen = screen
        this.topLayout = topLayout
    }

    render(message, timeout) {
        const messageDialog = this.blessed.message({
            parent: this.topLayout.render(),
            top: 'center',
            left: 'center',
            width: '50%',
            height: '30%',
            border: {
                type: 'line'
            },
        });

        messageDialog.display(message, timeout);

        return messageDialog
    }
}

module.exports = MessageDialog
