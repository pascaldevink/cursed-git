'use strict'

class TopLayout {
    constructor(blessed, screen) {
        this.blessed = blessed
        this.screen = screen
        this.box = this.createBox(blessed, screen)
    }

    createBox(blessed, screen) {
        const box = blessed.box({
            top: 'center',
            left: 'center',
            width: '100%',
            height: '100%',
            tags: true,
            border: {
                type: 'line'
            },
            style: {
                fg: 'white',
                bg: 'black',
                border: {
                    fg: '#f0f0f0'
                },
                hover: {
                    bg: 'green'
                }
            }
        });

        box.focus();
        screen.append(box);
        screen.render();

        return box;
    }

    render() {
        return this.box
    }
}

module.exports = TopLayout
