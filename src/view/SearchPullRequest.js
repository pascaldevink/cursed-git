'use strict'

const dotProp = require('dot-prop')

class SearchPullRequest {
    constructor(eventEmitter, blessed, screen, topLayout, chalk) {
        this.eventEmitter = eventEmitter
        this.blessed = blessed
        this.screen = screen
        this.topLayout = topLayout
        this.chalk = chalk
    }

    render(pullRequests, labelColors) {
        this.screen.realloc()

        const keyboardHelper = this.blessed.text({
            top: '100%-3',
            height: 3,
            width: '100%',
            border: {
                type: 'line'
            },
            style: {
                fg: 'white',
                bg: 'black',
                border: {
                    fg: '#f0f0f0'
                },
            },
            content: 'c = checkout, r = refresh, s = search, enter = details, q = quit',
        })
        this.screen.append(keyboardHelper)

        const prList = this.blessed.list({
            parent: this.topLayout.render(),
            top: 0,
            left: 0,
            height: '100%-3',
            type: 'overlay',
            style: {
                item: {
                    fg: 'white',
                    bg: 'black',
                },
                selected: {
                    fg: 'white',
                    bg: 'grey',
                }
            },
            mouse: true,
            keys: true,
            items: pullRequests.map((pullRequest) => {
                return pullRequest.title + pullRequest.labels.map((label) => {
                    const labelColor = labelColors.has(label) ? labelColors.get(label) : 'white'
                    const fn = dotProp.get(this.chalk, labelColor)
                    return ' - ' + fn(label)
                })
            })
        })

        prList.on('select', (selected, index) => {
            this.eventEmitter.emit('select pull request', index)
        })

        prList.on('keypress', (ch, key) => {
            switch(key.name) {
                case 'up':
                    prList.enterSelected()
                    break
                case 'down':
                    prList.enterSelected()
                    break
                case 'enter':
                    this.eventEmitter.emit('display details')
                    break
                case 'c':
                    this.eventEmitter.emit('checkout branch')
                    break
                case 'r':
                    this.eventEmitter.emit('refresh list')
                    break
                case 's':
                    this.eventEmitter.emit('prompt search query')
                    break
            }
        })

        prList.focus()
        this.screen.render()
        prList.enterSelected()
    }
}

module.exports = SearchPullRequest
