'use strict'

class SearchQueryPrompt {
    constructor(eventEmitter, blessed, screen, topLayout) {
        this.eventEmitter = eventEmitter
        this.blessed = blessed
        this.screen = screen
        this.topLayout = topLayout
    }

    render(currentSearchQuery) {
        const prompt = this.blessed.prompt({
            parent: this.topLayout.render(),
            top: 'center',
            left: 'center',
            width: '50%',
            height: 10,
            border: {
                type: 'line'
            },
        });

        prompt.readInput('Search query:', currentSearchQuery, (error, newSearchQuery) => {
            prompt.destroy()
            this.screen.render()

            if (null === error && newSearchQuery !== null) {
                this.eventEmitter.emit('update search query', newSearchQuery)
            }
        });

        return prompt
    }
}

module.exports = SearchQueryPrompt
