'use strict'

class LoadingScreen {
    constructor(blessed, screen, topLayout) {
        this.blessed = blessed
        this.screen = screen
        this.topLayout = topLayout
    }

    render(loadingMessage) {
        const loadingBox = this.blessed.loading({
            parent: this.topLayout.render(),
            top: 'center',
            left: 'center',
            width: '50%',
            height: '30%',
            border: {
                type: 'line'
            },
        });

        loadingBox.load(loadingMessage);

        return loadingBox
    }
}

module.exports = LoadingScreen
