'use strict'

class PullRequest {
    constructor(number, userName, repositoryUrl, title, body, branch) {
        this._number = number
        this._userName = userName
        this._repositoryUrl = repositoryUrl
        this._title = title
        this._body = body
        this._branch = branch
    }

    get number() {
        return this._number
    }

    get userName() {
        return this._userName
    }

    get repositoryUrl() {
        return this._repositoryUrl
    }

    get repositoryOwner() {
        const url = new URL(this._repositoryUrl);
        return url.pathname.split('/')[2]
    }

    get repository() {
        const url = new URL(this._repositoryUrl);
        return url.pathname.split('/')[3]
    }

    get title() {
        return this._title
    }

    get body() {
        return this._body
    }

    get branch() {
        return this._branch
    }
}

module.exports = PullRequest
