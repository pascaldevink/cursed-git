# Cursed-git

[![build status](https://gitlab.com/pascaldevink/cursed-git/badges/master/build.svg)](https://gitlab.com/pascaldevink/cursed-git/commits/master)
[![npm version](https://badge.fury.io/js/cursed-git.svg)](https://badge.fury.io/js/cursed-git)
[![npm downloads](https://img.shields.io/npm/dm/cursed-git.svg)](https://img.shields.io/npm/dm/cursed-git.svg)

Cursed-git is a tool to easily search github (it's currently the only supported 
provider) for issues/pull requests and to check them out. This makes reviewing
code changes much quicker than manually having to check out the website and 
copy/pasting the branch every time.

## How to install
```
npm install --global cursed-git
```
Once installed, you can install updates with
```
npm update --global cursed-git
```

## How it works
Start it from anywhere, but preferably in a git repository with:
```
cursed-git
```

On the first start, it will ask you for your github token, which you can create on
https://github.com/settings/tokens.
After that it will display the search results of the default query `is:open is:pr`
which you can customize by pressing the `s` key.

If you want to know the details of the pull request, select one from the list and
press `enter`. `esc` takes you back to the list and pressing `c` will checkout the
selected pull request. 

:warning: **Note** A branch can only be checked out if the repository that it belongs to and
the repository you started cursed-git in matches. 

## Development

Always do an `npm install` after updating your branch, to make sure you have the 
latest dependencies installed.

### Tests
Tests can be run with 
```
npm test
```

### Publishing
Publishing to npmjs is as easy as 
```
npm publish
```
