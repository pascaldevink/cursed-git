'use strict'

class MapBasedConfigurationStore {
    constructor() {
        this._configuration = new Map()
    }

    set(key, value) {
        this._configuration.set(key, value)
    }

    has(key) {
        return this._configuration.has(key)
    }

    get(key) {
        return this._configuration.get(key)
    }
}

module.exports = MapBasedConfigurationStore;
