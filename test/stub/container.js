'use strict'

class MapBasedContainer {
    constructor() {
        this._services = new Map()
    }

    set(name, value) {
        this._services.set(name, value)
    }

    use(name) {
        return this._services.get(name)
    }
}

module.exports = MapBasedContainer
