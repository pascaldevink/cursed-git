"use strict"

const assert = require('assert')
const sinon = require('sinon')
const mock = require('mock-require')

const EventEmitter = require('events')
const ConfigurationStore = require('../../src/service/ConfigurationStore')
const MapBasedConfigurationStore = require('../stub/configstore')

const IntroductionController = require('../../src/controller/Introduction')

describe('introduction', function() {
    it('should ask for a github token when it is not available in configuration', function(done) {
        // Arrange
        const eventEmitter = new EventEmitter()
        const mapBasedConfigurationStore = new MapBasedConfigurationStore()
        const configurationStore = new ConfigurationStore(mapBasedConfigurationStore)

        mock('inquirer', { prompt: () => {
            return new Promise((callback) => {
                callback({github_token: 'abcdef'})
            })
        } })
        const inquirer = require('inquirer')
        
        // Assert
        assert.equal(false, configurationStore.has('github_token'))

        eventEmitter.on('reroute', (route) => {
            assert.equal('search', route.name)
            assert.equal('abcdef', configurationStore.get('github_token'))

            done()
        })

        // Act
        const introductionController = new IntroductionController(eventEmitter, inquirer, configurationStore)
        introductionController.action()
    })

    it('should not ask for a github token when it is available in configuration', function(done) {
        // Arrange
        const eventEmitter = new EventEmitter()
        const mapBasedConfigurationStore = new MapBasedConfigurationStore()
        const configurationStore = new ConfigurationStore(mapBasedConfigurationStore)
        configurationStore.set('github_token', 'abcdef12345')
        
        mock('inquirer', { prompt: () => {
            return new Promise(() => {
                done(new Error('Inquirer prompt was not expected'));
            })
        } })
        const inquirer = require('inquirer')

        // Assert
        eventEmitter.on('reroute', (route) => {
            assert.equal('search', route.name)
            assert.equal('abcdef12345', configurationStore.get('github_token'))

            done()
        })

        // Act
        const introductionController = new IntroductionController(eventEmitter, inquirer, configurationStore)
        introductionController.action()
    })
})
