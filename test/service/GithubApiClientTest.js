'use strict'

const assert = require('assert')

const GithubApiClient = require('../../src/service/GithubApiClient')

describe('GithubApiClient', function() {
    it('should search and return a list of pull requests', function(done) {
        // Arrange
        const searchQuery = "is:open is:pr"

        const githubApi = {
            search: {
                issues(query, callback) {
                    assert.equal(searchQuery, query.q)

                    callback(null, {
                        items: [
                            {
                                number: 123,
                                title: 'foo',
                                body: 'foobar',
                                repository_url: 'https://api.github.com/repos/pascaldevink/cursed-git',
                                user: {
                                    login: 'pascaldevink'
                                },
                                labels: [],
                            }
                        ]
                    })
                }
            }
        }

        const gitClient = {}

        // Act
        const githubApiClient = new GithubApiClient(githubApi, gitClient)
        const searchPromise = githubApiClient.search(searchQuery)

        // Assert
        assert(searchPromise instanceof Promise)
        searchPromise.then((pullRequests) => {
            assert.equal(1, pullRequests.length)
            assert.equal(123, pullRequests[0].number)
            done()
        }).catch(() => {
            done(new Error('An error occurred while not expected'))
        })
    })

    it('should handle errors with grace after searching', function(done) {
        // Arrange
        const searchQuery = "is:open is:pr"

        const githubApi = {
            search: {
                issues(query, callback) {
                    assert.equal(searchQuery, query.q)

                    callback({}, null)
                }
            }
        }

        const gitClient = {}

        // Act
        const githubApiClient = new GithubApiClient(githubApi, gitClient)
        const searchPromise = githubApiClient.search(searchQuery)

        // Assert
        assert(searchPromise instanceof Promise)
        searchPromise.then(() => {
            done(new Error('An error did not occur while expected'))
        }).catch((error) => {
            assert.notStrictEqual({}, error)
            done()
        })
    })

    it('should return pull request information', function(done) {
        // Arrange
        const githubApi = {
            pullRequests: {
                get(pullRequest, callback) {
                    assert.equal('pascaldevink', pullRequest.owner)
                    assert.equal('cursed-git', pullRequest.repo)
                    assert.equal('123', pullRequest.number)

                    callback(null, {
                        number: 123,
                        title: 'foo',
                        body: 'foobar',
                        repository_url: 'https://api.github.com/repos/pascaldevink/cursed-git',
                        user: {
                            login: 'pascaldevink'
                        },
                        head: {
                            ref: 'master'
                        }
                    })
                }
            }
        }

        const gitClient = {}

        // Act
        const githubApiClient = new GithubApiClient(githubApi, gitClient)
        const pullRequestPromise = githubApiClient.getPullRequestInformation(
            'pascaldevink',
            'cursed-git',
            123
        )

        // Assert
        assert(pullRequestPromise instanceof Promise)
        pullRequestPromise.then((pullRequest) => {
            assert(123, pullRequest.number)

            done()
        })
    })

    it('should handle errors with grace after getting pull request information', function(done) {
        // Arrange
        const githubApi = {
            pullRequests: {
                get(pullRequest, callback) {
                    assert.equal('pascaldevink', pullRequest.owner)
                    assert.equal('cursed-git', pullRequest.repo)
                    assert.equal('123', pullRequest.number)

                    callback({}, null)
                }
            }
        }

        const gitClient = {}

        // Act
        const githubApiClient = new GithubApiClient(githubApi, gitClient)
        const pullRequestPromise = githubApiClient.getPullRequestInformation(
            'pascaldevink',
            'cursed-git',
            123
        )

        // Assert
        assert(pullRequestPromise instanceof Promise)
        pullRequestPromise.then(() => {
            done(new Error('An error did not occur while expected'))
        }).catch((error) => {
            assert.notStrictEqual({}, error)
            done()
        })
    })

    it('should checkout pull request branch', function(done) {
        // Arrange
        const githubApi = {
            pullRequests: {
                get(pullRequest, callback) {
                    callback(null, {
                        number: 123,
                        title: 'foo',
                        body: 'foobar',
                        repository_url: 'https://api.github.com/repos/pascaldevink/cursed-git',
                        user: {
                            login: 'pascaldevink'
                        },
                        labels: [],
                        head: {
                            ref: 'feature/add_tests'
                        }
                    })
                }
            }
        }

        const gitClient = {
            fetch(callback) {
                callback(null, "done")
            },
            checkout(branch, callback) {
                callback(null, "done")
            }
        }

        // Act
        const githubApiClient = new GithubApiClient(githubApi, gitClient)
        const checkoutPullRequestPromise = githubApiClient.checkoutPullRequest(
            'pascaldevink',
            'cursed-git',
            123
        )

        // Assert
        assert(checkoutPullRequestPromise instanceof Promise)
        checkoutPullRequestPromise.then(() => {
            done()
        })
    })

    it('should return pull request errors when checking out a branch', function(done) {
        // Arrange
        const githubApi = {
            pullRequests: {
                get(pullRequest, callback) {
                    callback({}, null)
                }
            }
        }

        const gitClient = {}

        // Act
        const githubApiClient = new GithubApiClient(githubApi, gitClient)
        const checkoutPullRequestPromise = githubApiClient.checkoutPullRequest(
            'pascaldevink',
            'cursed-git',
            123
        )

        // Assert
        assert(checkoutPullRequestPromise instanceof Promise)
        checkoutPullRequestPromise.then(() => {
            done(new Error('An error did not occur while expected'))
        }).catch((error) => {
            assert.notStrictEqual({}, error)
            done()
        })
    })

    it('should return fetch errors when checking out a branch', function(done) {
        // Arrange
        const githubApi = {
            pullRequests: {
                get(pullRequest, callback) {
                    callback(null, {
                        number: 123,
                        title: 'foo',
                        body: 'foobar',
                        repository_url: 'https://api.github.com/repos/pascaldevink/cursed-git',
                        user: {
                            login: 'pascaldevink'
                        },
                        labels: [],
                        head: {
                            ref: 'feature/add_tests'
                        }
                    })
                }
            }
        }

        const gitClient = {
            fetch(callback) {
                callback({}, null)
            }
        }

        // Act
        const githubApiClient = new GithubApiClient(githubApi, gitClient)
        const checkoutPullRequestPromise = githubApiClient.checkoutPullRequest(
            'pascaldevink',
            'cursed-git',
            123
        )

        // Assert
        assert(checkoutPullRequestPromise instanceof Promise)
        checkoutPullRequestPromise.then(() => {
            done(new Error('An error did not occur while expected'))
        }).catch((error) => {
            assert.notStrictEqual({}, error)
            done()
        })
    })

    it('should return checkout errors when checking out a branch', function(done) {
        // Arrange
        const githubApi = {
            pullRequests: {
                get(pullRequest, callback) {
                    callback(null, {
                        number: 123,
                        title: 'foo',
                        body: 'foobar',
                        repository_url: 'https://api.github.com/repos/pascaldevink/cursed-git',
                        user: {
                            login: 'pascaldevink'
                        },
                        labels: [],
                        head: {
                            ref: 'feature/add_tests'
                        }
                    })
                }
            }
        }

        const gitClient = {
            fetch(callback) {
                callback(null, "done")
            },
            checkout(branch, callback) {
                callback({}, null)
            }
        }

        // Act
        const githubApiClient = new GithubApiClient(githubApi, gitClient)
        const checkoutPullRequestPromise = githubApiClient.checkoutPullRequest(
            'pascaldevink',
            'cursed-git',
            123
        )

        // Assert
        assert(checkoutPullRequestPromise instanceof Promise)
        checkoutPullRequestPromise.then(() => {
            done(new Error('An error did not occur while expected'))
        }).catch((error) => {
            assert.notStrictEqual({}, error)
            done()
        })
    })
})
