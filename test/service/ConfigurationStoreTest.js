"use strict"

const assert = require('assert')

const ConfigurationStore = require('../../src/service/ConfigurationStore')
const MapBasedConfigurationStore = require('../stub/configstore')

describe('ConfigurationStore', function() {
    beforeEach(function() {
        const mapBasedConfigurationStore = new MapBasedConfigurationStore()
        this._configurationStore = new ConfigurationStore(mapBasedConfigurationStore)
    });

    it('should return a set value', function() {
        this._configurationStore.set('foo', 'bar')
        assert.equal('bar', this._configurationStore.get('foo'))
    })

    it('should return whether it has a value for a given key', function() {
        this._configurationStore.set('foo', 'bar')
        assert.ok(this._configurationStore.has('foo'))
    })

    it('should use a default value if the key is unknown', function() {
        assert.equal(false, this._configurationStore.has('foo'))
        assert.equal('baz', this._configurationStore.get('foo', 'baz'))
    })
})
