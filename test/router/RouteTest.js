'use strict'

const assert = require('assert')

const Route = require('../../src/router/Route')

describe('Router', function() {
    it('should always return parameters as array', function() {
        assert.ok(Array.isArray(new Route('foo', 'bar').parameters))
        assert.ok(Array.isArray(new Route('foo', ['bar']).parameters))
        assert.ok(Array.isArray(new Route('foo', null).parameters))
        assert.ok(Array.isArray(new Route('foo').parameters))
    })
})
