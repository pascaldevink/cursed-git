'use strict'

const assert = require('assert')

const Route = require('../../src/router/Route')

const BasicRouter = require('../../src/router/BasicRouter')

describe('Routing', function () {
    it('should execute controller for matching route', function (done) {
        // Arrange
        const route = new Route('foo', 'foobar')

        const routes = {
            'foo': 'fooController'
        }

        const Ioc = require('adonis-fold').Ioc

        // Assert
        Ioc.bind('fooController', (app) => {
            return {
                action(parameter) {
                    assert.equal('foobar', parameter)
                    done()
                }
            }
        })

        // Act
        const basicRouter = new BasicRouter(Ioc, routes)
        basicRouter.route(route)
    })
})
