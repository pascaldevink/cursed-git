'use strict'

const assert = require('assert')

const EventEmitter = require('events')
const MapBasedContainer = require('../stub/container')
const Route = require('../../src/router/Route')

const AppKernel = require('../../src/kernel/AppKernel')

describe('AppKernel', function() {
    it('should return a promise when building the AppKernel', function() {
        const workingDirectory = process.cwd()
        const providers = []

        const appKernelPromise = AppKernel.build(workingDirectory, providers)

        assert(appKernelPromise instanceof Promise)
    })

    it('should set up the container and rerouting upon booting', function(done) {
        const originalRoute = new Route('foo')

        const container = new MapBasedContainer()
        container.set('cursed/event/EventEmitter', {
            on(name, callback) {
                assert.equal('reroute', name)
                callback(originalRoute)
            }
        })
        container.set('cursed/router/Router', {
            route(route) {
                assert.equal(originalRoute, route)
                done()
            }
        })

        const appKernel = new AppKernel(container)
        appKernel.boot()

        assert.equal(container, appKernel.container)
    })
})
